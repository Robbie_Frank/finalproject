﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadoutSelectionButtonManager : MonoBehaviour
{
    // References
    [SerializeField] LoadOutManager loadout;
    [SerializeField] TMP_Dropdown hardpoint1;
    [SerializeField] TMP_Dropdown hardpoint2;
    [SerializeField] TMP_Dropdown hardpoint3;
    [SerializeField] TMP_Dropdown hardpoint4;
    [SerializeField] TMP_Dropdown hardpoint5;
    [SerializeField] TMP_Dropdown hardpoint6;
    [SerializeField] TMP_Dropdown hardpoint7;
    [SerializeField] TMP_Dropdown hardpoint8;
    [SerializeField] TMP_Dropdown hardpoint9;

    // Adds all the selected weapons to the LoadOutManager and loads the mission 1 scene
    public void LaunchMission()
    {
        loadout.hardpoint1 = hardpoint1.value;
        loadout.hardpoint2 = hardpoint2.value;
        loadout.hardpoint3 = hardpoint3.value;
        loadout.hardpoint4 = hardpoint4.value;
        loadout.hardpoint5 = hardpoint5.value;
        loadout.hardpoint6 = hardpoint6.value;
        loadout.hardpoint7 = hardpoint7.value;
        loadout.hardpoint8 = hardpoint8.value;
        loadout.hardpoint9 = hardpoint9.value;
        SceneManager.LoadScene(4);
    }

    // Loads the mission briefing screen
    public void BacktoMissionBrief()
    {
        SceneManager.LoadScene(2);
    }
}