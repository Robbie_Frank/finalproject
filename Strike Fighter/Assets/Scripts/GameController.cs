﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    // Fields
    int primaryObjectivesRemaining = 3;
    public bool missionEnded = false;

    // References
    [Header("UI")]
    [SerializeField] TextMeshProUGUI gameEndText;
    [SerializeField] GameObject returnButton;

    // Triggers the end of a game and has an argument for what triggered the end and changes the message displayed based on the passed argument
    public void GameEnd(string condition)
    {
        missionEnded = true;
        if (condition == "Winchester")
        {
            gameEndText.text = "You have ran out of ammunition for all your weapons and therefore cannot destroy the primary targets. Mission is considered a failure. Return to base.";
        }
        else if (condition == "Targets Destroyed")
        {
            gameEndText.text = "You have successfully destroyed all primary targets. Mission objectives have been completed, return to base for your debrief.";
        }
        else if (condition == "Fatal Aircraft Damage")
        {
            gameEndText.text = "Your aircraft has sustained too much damage to complete your primary objectives. Mission is considered a failure. Return to base.";
        }
        else if (condition == "Out of Bounds")
        {
            gameEndText.text = "You have left the area of operations. Mission failure. Return to base.";
        }

        // If gameEndText exists, it displays it so the player can see the ending message
        if (gameEndText.gameObject != null)
        {
            gameEndText.GetComponent<TextMeshProUGUI>().enabled = true;
        }

        // Shows the button to return back to the mission briefing
        returnButton.SetActive(true);
        // Creates an array of all the SAM Launchers in the game
        GameObject[] SAMs = GameObject.FindGameObjectsWithTag("SAM");

        // If there are SAMs in the scene, they are all destroyed one at a time
        // This is done to stop any audio from the missiles
        if (SAMs != null)
        {
            foreach (GameObject SAM in SAMs)
            {
                Destroy(SAM);
            }
        }

        // Game is paused
        Time.timeScale = 0;
    }

    // Unpauses the game and loads the mission selection scene
    public void ReturnButton()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(2);
    }

    // Decreases the amount of remaining mission objectives left to destroy and ends the game when they're all destroyed
    public void ObjectiveDestroyed()
    {
        primaryObjectivesRemaining--;
        if (primaryObjectivesRemaining <= 0)
        {
            if (!missionEnded)
            {
                GameEnd("Targets Destroyed");
            }
        }
    }
}