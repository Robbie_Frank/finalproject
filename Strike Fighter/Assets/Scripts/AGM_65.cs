﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AGM_65 : MonoBehaviour
{
    // Fields
    [SerializeField] float speed;

    // References
    [SerializeField] PlayerController player;
    [SerializeField] GameObject target;
    [SerializeField] GameObject explosion;
    Vector2 targetLocation;

    void Start()
    {
        // Finds the player and links up their script with the player field
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        // Sets the target equal to that of the player
        target = player.target;

        // Sets the targetLocation equal to the position of the target
        // (Need to do this so if target is destroyed while missile is in flight, no error is thrown and missile continues on its path until impact)
        targetLocation = new Vector2(target.transform.position.x, target.transform.position.y);

        // Orients the missile to point towards its target
        transform.right = target.transform.position - transform.position;
    }

    void Update()
    {
        // Moves the missile towards the targetLocation
        transform.position = Vector2.MoveTowards(transform.position, targetLocation, speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // If the missile impacts the ground, it spawns an explosion and the missile is destroyed
        if (collision.gameObject.tag == ("Ground"))
        {
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}