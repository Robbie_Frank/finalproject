﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MissionSelectButtonManager : MonoBehaviour
{
    // Loads the Mission1 Briefing Scene
    public void ProceedToMission1Briefing()
    {
        SceneManager.LoadScene(2);
    }

    // Returns to the main menu
    public void BackToMainMenu()
    {
        SceneManager.LoadScene(0);
    }
}