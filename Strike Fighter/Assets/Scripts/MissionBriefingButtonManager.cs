﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class MissionBriefingButtonManager : MonoBehaviour
{
    // Fields
    string mission1Brief = "NATO ground forces are planning an attack on an enemy forward operating base. " +
        "It is imperitive that they have access to air support during their operation. " +
        "The enemy has placed surface to air missile launchers and anti-aircraft artillery along the planned flight route. " +
        "Intel reports that there are 3 SAM launchers and 6 AAA guns. You are tasked with destroying the enemy air defenses. " +
        "You MUST destroy the SAM launchers so our air units can safely support our ground troops during their operation. " +
        "Destroying the AAA guns is a secondary optional objective. " +
        "Because you will be operating behind enemy lines rearming and refueling will not be possible during this mission. " +
        "Choose your loadout wisely and conserve your ammunition to ensure you can destroy all the SAM launchers. " +
        "Intel also reports no enemy air activity on radar and our recon teams on the ground report no activity at enemy air bases. " +
        "The skies should be clear of all enemy aircraft.";
    List<char> briefSoFar;
    [SerializeField] float typingSpeed;

    // References
    [SerializeField] TextMeshProUGUI briefText;

    private void Start()
    {
        // Runs the TypeText couroutine to start typing out the briefing
        StartCoroutine(TypeText());
    }

    // Loads the Loadout Selection scene
    public void ProceedToLoadoutSelection()
    {
        SceneManager.LoadScene(3);
    }

    // Loads the mission selection screen
    public void BackToMissionSelect()
    {
        SceneManager.LoadScene(1);
    }

    private void Update()
    {
        // If the player clicks the left mouse button the coroutine is stopped, and the mission briefing text is fully displayed, skipping the typing animation
        if (Input.GetMouseButtonDown(0))
        {
            StopAllCoroutines();
            briefText.text = mission1Brief;
        }
    }

    // Method to make the mission briefing text animated to appear one character at a time
    IEnumerator TypeText()
    {
        // Looks at each character in the mission1Brief string one at a time and adds them to the briefText UI object, waits for a little, then adds the next character
        // and continues until all have been added
        for(int i = 0; i < mission1Brief.Length; i++)
        {
            briefText.text += mission1Brief[i];
            yield return new WaitForSeconds(typingSpeed);
        }
    }
}