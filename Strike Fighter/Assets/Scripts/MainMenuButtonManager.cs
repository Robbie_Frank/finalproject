﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtonManager : MonoBehaviour
{
    // Loads the mission select scene
    public void StartGame()
    {
        SceneManager.LoadScene(1);
    }

    // Quits the entire game and exits to the desktop
    public void QuitGame()
    {
        Application.Quit();
    }

}