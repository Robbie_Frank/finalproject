﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SAMLauncher : MonoBehaviour
{
    // Fields
    [SerializeField] int ammoCount = 3;
    bool readyToFire = true;
    float preperationTimer = 10f;
    int health = 100;

    // References
    [SerializeField] PlayerController player;
    [SerializeField] Transform missileLocation;
    [SerializeField] GameObject s_75;
    [SerializeField] GameController gameController;
    [SerializeField] GameObject smoke;

    private void Start()
    {
        // Searches for and links up with the Player in the scene
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    private void Update()
    {
        // Checks to see if the player is within range and if so, attempts to fire a missile at the player
        if (Vector2.Distance(player.gameObject.transform.position, gameObject.transform.position) < 38)
        {
            if (readyToFire && ammoCount > 0)
            {
                LaunchSAM();
            }
        }

        // Checks to see if missile is ready to fire, if not, updates the cooldown timer until missile is ready to fire again
        if (!readyToFire)
        {
            preperationTimer -= Time.deltaTime;
            if (preperationTimer <= 0)
            {
                readyToFire = true;
                preperationTimer = 10f;
            }
        }
    }

    // When this game object is left clicked on, it sets it as the player's target
    private void OnMouseDown()
    {
        player.target = gameObject;
    }

    // Launches a missile
    void LaunchSAM()
    {
        Instantiate(s_75, missileLocation.position, missileLocation.rotation);
        ammoCount--;
        readyToFire = false;
    }

    // Applies damage to this game object and destroys it if needed
    public void TakeDamage(int amount)
    {
        health -= amount;
        if (health <= 0)
        {
            Instantiate(smoke, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }

    // Tells the game controller when this game object is destroyed
    private void OnDestroy()
    {
        if (!gameController.missionEnded)
        gameController.ObjectiveDestroyed();
    }
}