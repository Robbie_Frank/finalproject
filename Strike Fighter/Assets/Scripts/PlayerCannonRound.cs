﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCannonRound : MonoBehaviour
{
    // Fields
    float velocity = 15f;
    float lifeTime = 10;

    // References
    Rigidbody2D rb2d;
    

    private void Start()
    {
        // Sets this game objects rigidbody component to the rb2d field
        rb2d = GetComponent<Rigidbody2D>();

        // Applies an instant force to the bullet to propel it forward upon it being spawned
        rb2d.AddForce(transform.up * velocity, ForceMode2D.Impulse);
    }

    private void Update()
    {
        // Updates the lifetime counter, when it reaches 0, the round is destroyed
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Checks to see if the rounds impacted an enemy on the ground
        if (collision.gameObject.layer == 12)
        {
            // If the round impacts an AAA gun, deals damage and the round is destroyed
            if (collision.gameObject.name.StartsWith("A"))
            {
                collision.gameObject.GetComponent<AAAGun>().TakeDamage(25);
                Destroy(gameObject);
            }
            // If the round impacts a SAM Launcher, deals damage and the round is destroyed
            else if (collision.gameObject.name.StartsWith("S"))
            {
                collision.gameObject.GetComponent<SAMLauncher>().TakeDamage(15);
                Destroy(gameObject);
            }
        }
        // If the round impacts anything else, the round is simply destroyed
        else
        {
            Destroy(gameObject);
        }
    }
}