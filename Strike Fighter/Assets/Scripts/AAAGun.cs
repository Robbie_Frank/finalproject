﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AAAGun : MonoBehaviour
{
    // Fields
    int health = 100;
    float fireRate = 0.25f;
    bool readyToFire = true;

    // References
    [SerializeField] PlayerController player;
    [SerializeField] GameObject cannonRound;
    [SerializeField] GameObject muzzle;
    [SerializeField] GameObject smoke;
    

    private void Start()
    {
        // Searches for the player and links them up with this game object
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    private void Update()
    {
        // Checks to see if the player is in range and opens fire on them if they are
        if (Vector2.Distance(player.gameObject.transform.position, gameObject.transform.position) < 38)
        {
            if (readyToFire)
            {
                Instantiate(cannonRound, muzzle.transform.position, transform.rotation);
                readyToFire = false;
            }
        }

        // If gun isn't ready to fire, starts the cooldown timer until gun is ready to fire again
        if (!readyToFire)
        {
            fireRate -= Time.deltaTime;
            if (fireRate <= 0)
            {
                readyToFire = true;
                fireRate = 0.25f;
            }
        }

    }

    // When this game object is left clicked on, it is set as the player's target
    private void OnMouseDown()
    {
        player.target = gameObject;
    }

    // Applies damage to this game object and destroys it when ready
    public void TakeDamage(int amount)
    {
        health -= amount;
        if (health <= 0)
        {
            Instantiate(smoke, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}