﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerController : MonoBehaviour
{
    // Fields
    [Header("Plane Information")]
    [SerializeField] float minimumSpeed;
    [SerializeField] float maximumSpeed;
    [SerializeField] float climbSpeed;
    int health = 100;
    float outOfBoundsTimer = 7.5f;
    bool outOfBounds = false;

    [Header("Weapons")]
    [SerializeField] int cannonCount = 412;
    [SerializeField] float fireRate = 0.05f;
    bool canFireCannon = true;
    [SerializeField] int agm_65Count;
    [SerializeField] int gbu_12Count;
    [SerializeField] int aim_9Count = 2;
    [SerializeField] int aim_120Count = 2;
    [SerializeField] int counterMeasuresCount = 10;
    bool agm_65Selected = true;
    bool gbu_12Selected = false;
    bool aim_9Selected = false;
    bool aim_120Selected = false;

    [Header("Audio")]
    bool altitudeWarningPlaying = false;
    
    // References
    [Header("Audio")]
    [SerializeField] AudioSource altitudeVoiceWarning;
    [SerializeField] AudioSource altitudeAudioWarning;
    [SerializeField] AudioSource cockpitNoise;
    [SerializeField] AudioSource explosion;

    [Header("Projectiles/Weapons")]
    [SerializeField] GameObject gbu_12;
    [SerializeField] GameObject agm_65;
    [SerializeField] GameObject flare;
    [SerializeField] GameObject cannonRound;
    [SerializeField] Transform underWingHardpoint;
    [SerializeField] Transform wingtipHardpoint;
    LoadOutManager loadout;
    public GameObject target;

    [Header("UI")]
    [SerializeField] TextMeshProUGUI selectedWeaponText;
    [SerializeField] TextMeshProUGUI agmAmmoText;
    [SerializeField] TextMeshProUGUI gbuAmmoText;
    [SerializeField] TextMeshProUGUI healthText;
    [SerializeField] TextMeshProUGUI flaresText;
    [SerializeField] TextMeshProUGUI cannonAmmoText;
    [SerializeField] TextMeshProUGUI returnToMissionText;

    [Header("Game Controller")]
    [SerializeField] GameController gameController;

    void Start()
    {
        // Grabs loadout info from the Loadout Selection Screen and applies it to the aircraft
        loadout = GameObject.FindGameObjectWithTag("LoadoutManager").GetComponent<LoadOutManager>();
        if (loadout.hardpoint2 == 0)
        {
            agm_65Count++;
        }
        else
        {
            gbu_12Count += 2;
        }
        if (loadout.hardpoint3 == 1)
        {
            agm_65Count++;
        }
        else
        {
            gbu_12Count += 2;
        }
        if (loadout.hardpoint7 == 1)
        {
            agm_65Count++;
        }
        else
        {
            gbu_12Count += 2;
        }
        if (loadout.hardpoint8 == 0)
        {
            agm_65Count++;
        }
        else
        {
            gbu_12Count += 2;
        }

        // Updates all the UI with current ammo count for all weapons
        agmAmmoText.text = "AGM-65E Ammo: " + agm_65Count.ToString();
        gbuAmmoText.text = "GBU-12 Ammo: " + gbu_12Count.ToString();
        healthText.text = "Hull Integrity: " + health.ToString();
        cannonAmmoText.text = "Cannon Ammo: " + cannonCount.ToString();
        flaresText.text = "Flares Remaining " + counterMeasuresCount.ToString();
    }

    void Update()
    {
        // Grabs the vertical amount player is trying to move
        float vertical = Input.GetAxis("Vertical") * climbSpeed;

        // Moves the player forward
        transform.Translate(Vector2.right * minimumSpeed * Time.deltaTime);

        // Rotates the player as they pitch up and down
        transform.Rotate(new Vector3(0, 0, vertical * climbSpeed * Time.deltaTime));

        // Starts out of bounds countdown timer if out of bounds and ends game if the timer reaches 0
        if (outOfBounds)
        {
            outOfBoundsTimer -= Time.deltaTime;
            if (outOfBoundsTimer <= 0)
            {
                returnToMissionText.gameObject.SetActive(false);
                gameController.missionEnded = true;
                altitudeAudioWarning.Stop();
                altitudeVoiceWarning.Stop();
                gameController.GameEnd("Out of Bounds");
            }
        }

        // Updates the cooldown for the firing of the cannon
        if (!canFireCannon)
        {
            fireRate -= Time.deltaTime;
            if (fireRate <= 0)
            {
                canFireCannon = true;
                fireRate = 0.05f;
            }
        }

        // Checks if altitude is too low and plays altitude warning messages and stops them when a safe altitude has been established
        if (transform.position.y <= -2f && !altitudeWarningPlaying)
        {
            altitudeVoiceWarning.Play();
            altitudeAudioWarning.Play();
            altitudeWarningPlaying = true;
        }
        else if (transform.position.y > -2f && altitudeWarningPlaying)
        {
            altitudeVoiceWarning.Stop();
            altitudeAudioWarning.Stop();
            altitudeWarningPlaying = false;
        }

        // Attempts to release special weapon when the weapon release button is pressed
        if (Input.GetButtonDown("Weapon Release"))
        {
            ReleaseWeapon();
        }
        
        // Attemps to release flares when the Countermeasures switch is pressed
        if (Input.GetButtonDown("CounterMeasures"))
        {
            ReleaseCounterMeasures();
        }

        // Attempts to fire the cannon when the trigger is depressed
        if (Input.GetButton("Trigger"))
        {
            if (canFireCannon)
            {
                FireCannon();
            }
        }

        // Switches to the AGM-65E as the selected weapon when 1 is pressed
        if (Input.GetKeyDown("1"))
        {
            agm_65Selected = true;
            gbu_12Selected = false;
            selectedWeaponText.text = "Selected Weapon: AGM-65E";
        }

        // Switches to the GBU-12 as the selected weapon when 2 is pressed
        if (Input.GetKeyDown("2"))
        {
            agm_65Selected = false;
            gbu_12Selected = true;
            selectedWeaponText.text = "Selected Weapon: GBU-12";
        }
    }

    // Releases the currently selected special weapon
    void ReleaseWeapon()
    {
        if (gbu_12Selected && gbu_12Count > 0 && target != null)
        {
            Instantiate(gbu_12, underWingHardpoint.position, underWingHardpoint.rotation);
            gbu_12Count--;
            gbuAmmoText.text = "GBU-12 Ammo: " + gbu_12Count.ToString();
        }
        else if (agm_65Selected && agm_65Count > 0 && target != null)
        {
            Instantiate(agm_65, wingtipHardpoint.position, wingtipHardpoint.rotation);
            agm_65Count--;
            agmAmmoText.text = "AGM-65E Ammo: " + agm_65Count.ToString();
        }
    }

    // Fires the cannon
    void FireCannon()
    {
        if (cannonCount > 0 && canFireCannon)
        {
            Instantiate(cannonRound, transform.position, Quaternion.Euler(0, 0, transform.eulerAngles.z - 90));
            cannonCount--;
            cannonAmmoText.text = "Cannon Ammo: " + cannonCount.ToString();
            canFireCannon = false;
        }
    }

    // Releases flares
    void ReleaseCounterMeasures()
    {
        if (counterMeasuresCount > 0)
        {
            Instantiate(flare, transform.position, transform.rotation);
            counterMeasuresCount--;
            flaresText.text = "Flares Remaining: " + counterMeasuresCount.ToString();
        }
    }

    // Deals damage to the player and checks if they have been destroyed
    public void TakeDamage(int amount)
    {
        health -= amount;
        healthText.text = "Hull Integrity: " + health.ToString() + "%";
        if (health <= 0)
        {
            altitudeAudioWarning.Stop();
            altitudeVoiceWarning.Stop();
            cockpitNoise.Stop();
            explosion.Play();
            gameController.missionEnded = true;
            gameController.GameEnd("Fatal Aircraft Damage");
        }
    }

    // Checks for collision with the ground and ends the mission if hit
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            altitudeAudioWarning.Stop();
            altitudeVoiceWarning.Stop();
            cockpitNoise.Stop();
            explosion.Play();
            gameController.missionEnded = true;
            gameController.GameEnd("Fatal Aircraft Damage");
        }
    }

    // Checks for exiting the map limit
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Map Limit")
        {
            outOfBounds = true;
            returnToMissionText.gameObject.SetActive(true);
        }
    }

    // Checks for re-entering the map limits
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Map Limit")
        {
            outOfBounds = false;
            returnToMissionText.gameObject.SetActive(false);
            outOfBounds = false;
            outOfBoundsTimer = 7.5f;
        }
    }
}