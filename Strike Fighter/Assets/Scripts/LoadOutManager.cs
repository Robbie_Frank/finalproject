﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadOutManager : MonoBehaviour
{
    // Fields (used to store which weapons were selected in the Loadout Selection scene)
    public int hardpoint1;
    public int hardpoint2;
    public int hardpoint3;
    public int hardpoint4;
    public int hardpoint5;
    public int hardpoint6;
    public int hardpoint7;
    public int hardpoint8;
    public int hardpoint9;

    private void Awake()
    {
        // Ensures that this game object persists between scenes
        DontDestroyOnLoad(gameObject);
    }
}