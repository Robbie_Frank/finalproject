﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AGM_65Explosion : MonoBehaviour
{
    // Fields
    float lifeTime = 4f;

    // References
    [SerializeField] GameObject explosion;

    private void Start()
    {
        // Spawns a particle effect for the explosion when created
        Instantiate(explosion, transform.position, explosion.transform.rotation);
    }

    private void Update()
    {
        // Updates the lifeTime counter and destroys the game object when the timer hits 0
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        // Checks to see if any ground enemies are caught in the explosion radius
        if (collision.gameObject.layer == 12)
        {
            // If an AAA gun is caught in the explosion, deals damage and the round is destroyed
            if (collision.gameObject.name.StartsWith("A"))
            {
                collision.gameObject.GetComponent<AAAGun>().TakeDamage(100);
            }
            // If a SAM Launcher is caught in the explosion, deals damage and the round is destroyed
            else if (collision.gameObject.name.StartsWith("S"))
            {
                collision.gameObject.GetComponent<SAMLauncher>().TakeDamage(100);
            }
        }
    }
}