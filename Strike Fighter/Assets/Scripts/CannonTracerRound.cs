﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonTracerRound : MonoBehaviour
{
    // Fields
    [Header("Round Specs")]
    float velocity = 7.5f;
    float lifeTime = 5;

    // References
    Rigidbody2D rb2d;
    [SerializeField] GameObject player;
    

    private void Start()
    {
        // Finds the player and links them up with the player field
        player = GameObject.FindGameObjectWithTag("Player");

        // Orients the round to face the player's position
        transform.up = player.transform.position - transform.position;

        // Sets rb2d to be the rigidbody2d component on this game object
        rb2d = GetComponent<Rigidbody2D>();

        // Applies an instant force to the round upon creation to push it forward
        rb2d.AddForce(transform.up * velocity, ForceMode2D.Impulse);
    }

    private void Update()
    {
        // Updates the lifeTime counter and destroys the game object when it reaches 0
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // If the round impacts the player, it deals damage to them and then the round is destroyed
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerController>().TakeDamage(20);
            Destroy(gameObject);
        }
    }
}