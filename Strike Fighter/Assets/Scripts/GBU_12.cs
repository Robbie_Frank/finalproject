﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GBU_12 : MonoBehaviour
{
    // Fields
    [SerializeField] float speed;

    // References
    [SerializeField] PlayerController player;
    [SerializeField] GameObject target;
    Rigidbody2D rb2d;
    [SerializeField] GameObject explosion;
    Vector2 targetLocation;

    void Start()
    {
        // Links the rigidbody2d component on this game object to the rb2d field
        rb2d = GetComponent<Rigidbody2D>();

        // Throws the bomb forward a bit to simulate it moving forward with the player's velocity upon release
        rb2d.AddForce(new Vector2(250, 0));

        // Searches for the player in the scene and gets a reference to their script
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        // Sets the target of the bomb to be the same as the player's
        target = player.target;

        // Sets the targetLocation field to be equal to the position of the target
        // (This is done so if the target is destroyed while the bomb is in flight, no error is thrown and the bomb impacts where the target WAS at before it was destroyed)
        targetLocation = new Vector2(target.transform.position.x, target.transform.position.y);
    }


    void Update()
    {
        // Moves the bomb towards the target using its speed field
        transform.position = Vector2.MoveTowards(transform.position, targetLocation, speed * Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // When the bomb hits the ground, it spanws an explosion and the bomb is destroyed
        if (collision.gameObject.tag == ("Ground"))
        {
            Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
        }
    }
}