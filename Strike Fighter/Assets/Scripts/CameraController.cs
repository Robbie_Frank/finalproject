﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    // Fields
    [SerializeField] float xOffset;
    [SerializeField] float yOffset;

    // References
    [SerializeField] GameObject player;

    void Update()
    {
        // Moves the camera along with the player
        transform.position = new Vector3(player.transform.position.x + xOffset, player.transform.position.y + yOffset, -10);
    }
}