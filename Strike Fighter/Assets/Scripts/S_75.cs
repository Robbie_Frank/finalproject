﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class S_75 : MonoBehaviour
{
    // Fields
    [Header("Missile Specs")]
    [SerializeField] float speed;

    // References
    [SerializeField] GameObject player;
    [SerializeField] GameObject target;
    [SerializeField] GameObject explosion;
    [SerializeField] GameObject flare;

    void Start()
    {
        // Searches for and finds the player game object for reference
        player = GameObject.FindGameObjectWithTag("Player");

        // Sets the player as the target
        target = player;
    }

    void Update()
    {
        // Searches the scene for any existing flares
        flare = GameObject.FindGameObjectWithTag("Flare");

        // If a flare is found the missile redirects to tracking the flare
        if (flare != null)
        {
            transform.position = Vector2.MoveTowards(transform.position, flare.transform.position, speed * Time.deltaTime);
            transform.up = flare.transform.position - transform.position;
        }
        // If no flare is found, the missile tracks the player
        else
        {
            transform.position = Vector2.MoveTowards(transform.position, target.transform.position, speed * Time.deltaTime);
            transform.up = target.transform.position - transform.position;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        // If missile impacts player, damage is applied and the missile is destroyed
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<PlayerController>().TakeDamage(100);
            Destroy(gameObject);
        }
        // If the missile impacts a flare, it destroys the flare and the missile itself
        else if (collision.gameObject.tag == "Flare")
        {
            Destroy(collision.gameObject);
            Destroy(gameObject);
        }
        // If the missile impacts the ground, the missile is destroyed
        else if (collision.gameObject.tag == "Ground")
        {
            Destroy(gameObject);
        }
    }
}