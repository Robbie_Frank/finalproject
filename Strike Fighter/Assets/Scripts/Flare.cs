﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Flare : MonoBehaviour
{
    // Fields
    float lifeTime = 7.5f;

    private void Update()
    {
        // Updates the lifeTime counter and destroys the flare when it reaches 0
        lifeTime -= Time.deltaTime;
        if (lifeTime <= 0)
        {
            Destroy(gameObject);
        }
    }
}